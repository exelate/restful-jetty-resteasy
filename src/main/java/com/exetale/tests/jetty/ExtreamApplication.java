/*
 * eXelate benchmarks suite to test the usage and performance of Java frameworks
 * for the next generation serving architecture.
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exetale.tests.jetty;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

/********************************************************
 * The configuration-less sample of HTTP REST application.
 */
public class ExtreamApplication extends Application {

	// List of services to expose
	private static Set<Object> services = new HashSet<Object>();

	public ExtreamApplication() {

		// initialize restful services
		services.add(new ExtreamApiService());
	}

	@Override
	public Set<Object> getSingletons() {
		return services;
	}

	public static Set<Object> getServices() {
		return services;
	}
}
