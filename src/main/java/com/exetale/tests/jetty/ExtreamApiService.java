/*
 * eXelate benchmarks suite to test the usage and performance of Java frameworks
 * for the next generation serving architecture.
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exetale.tests.jetty;

import java.net.URISyntaxException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/********************************************************
 * The Class ExtreamResource.
 */
@Path("/api")
public class ExtreamApiService {

    
    /**
     * Method to start extream server processing
     * @return
     */
    @GET @Path("/start")
    @Produces(MediaType.APPLICATION_JSON)
	public Response startExtream() throws URISyntaxException {		
    	
    	ResponseMessage msg = new ResponseMessage(0, "Jetty-Resteasy-Extream: Started", 34);
		return Response.ok(msg.toJson()).build();
		
	}
    
    /**
     * Method to stop extream server processing
     * @return
     */
    @GET @Path("/stop")
    @Produces(MediaType.APPLICATION_JSON)
    public Response stopExtream() {
    	
    	ResponseMessage msg = new ResponseMessage(0, "Jetty-Resteasy-Extream: Stopped", 34);
    	return Response.ok(msg.toJson()).build();
    }

    /**
     * Method to reload extream server processing
     * @return
     */
    @GET @Path("/reload")
    @Produces(MediaType.APPLICATION_JSON)
    public Response reloadExtream() {
    	
    	ResponseMessage msg = new ResponseMessage(0, "Jetty-Resteasy-Extream: Reloaded", 34);
    	return Response.ok(msg.toJson()).build();
    }
}
