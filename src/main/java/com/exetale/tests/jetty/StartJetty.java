/*
 * eXelate benchmarks suite to test the usage and performance of Java frameworks
 * for the next generation serving architecture.
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exetale.tests.jetty;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;

/********************************************************
 * The Class StartJetty starts the embedded server.
 */
public class StartJetty {
	
    /**
     * Create HTTP server exposing JAX-RS resources defined in this application.
     * @return Jetty HTTP server.
     */
    public static Server createServer(int port) {
        
        Server server = new Server(port);
        
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/extream");

		ServletHolder h = new ServletHolder(new HttpServletDispatcher());
		h.setInitParameter("javax.ws.rs.Application", "com.exetale.tests.jetty.ExtreamApplication");
		context.addServlet(h, "/*");

		server.setHandler(context);
		
		return server;
    }
    
    /**
     * Main entry point to run from command line
     * @param args
     * @throws Exception
     */
	public static void main(String[] args) throws Exception {
        
        Server server = createServer(8081);

		try {
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
